from .ngd_cg import NGD
from .adam_adaptive_ngd_cg import NaturalAdam
from .ams_adaptive_ngd_cg import NaturalAmsgrad
from .adagrad_adaptive_ngd_cg import NaturalAdagrad

from .ngd import NGDExact
from .adam_adaptive_ngd import NaturalAdamExact
